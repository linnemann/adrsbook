unit Person_BOM;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  tiObject;


type

  TPerson = class;
  TPersonList = class;

  { TPersonList }

  TPersonList = class(TtiObjectList)
    protected
      function  GetItems(i: integer): TPerson; reintroduce;
      procedure SetItems(i: integer; const AValue: TPerson); reintroduce;
    public
      property  Items[i:integer]: TPerson read GetItems write SetItems;
      function Add(const AObject: TPerson): integer; reintroduce;
      procedure Read; override;
      procedure Save; override;
  end;

  TPerson = class(TtiObject)
    private
      FFirstName: string;
      FLastName:  string;
      FInitials:  string;
      FTitle:     string;
      FNotes:     string;
      procedure SetFirstName(const AValue: string);
    protected
      function  GetParent: TPersonList; reintroduce;
    public
      property  Parent: TPersonList read GetParent;
      //procedure  Read; override;
      procedure  Save; override;
      function IsValid(const AErrors: TtiObjectErrors): boolean; override;
    published
      property Title: string read FTitle write FTitle;
      property FirstName: string read FFirstName write SetFirstName;
      property LastName: string read FLastName write FLastName;
      property Initials: string read FInitials write FInitials;
      property Notes: string read FNotes write FNotes;
  end;

implementation

{ TPersonList }

function TPersonList.GetItems(i: integer): TPerson;
begin
  result := TPerson(inherited GetItems(i));
end;

procedure TPersonList.SetItems(i: integer; const AValue: TPerson);
begin
  inherited SetItems(i, AValue);
end;

function TPersonList.Add(const AObject: Tperson): integer;
begin
  result:= inherited Add(AObject);
end;

procedure TPersonList.Read;
begin
  inherited;
  NotifyObservers;
end;

procedure TPersonList.Save;
begin
  inherited Save;
  NotifyObservers;
end;

{TPersonList}

procedure TPerson.SetFirstName(const AValue: string);
begin
  if FFirstName=AValue then exit;
  BeginUpdate;
  FFirstName:=AValue;
  if (Self.ObjectState <> posEmpty) then
    Self.Dirty:= True;
  EndUpdate;
end;

{TPerson}
function TPerson.GetParent: TPersonList;
begin
  result := TPersonList(inherited GetParent);
end;

procedure TPerson.Save;
begin
  inherited;
end;

function TPerson.IsValid(const AErrors: TtiObjectErrors): boolean;
begin
  Result:=inherited IsValid(AErrors);
  if Trim(FirstName) = '' then
    AErrors.AddError('FirstName', 'Firstname property is missing information', 1);

  if Trim(LastName) = '' then
    AErrors.AddError('LastName', 'Lastname property is missing information', 1);

  Result := (AErrors.Count = 0);
end;

end.

