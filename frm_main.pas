unit frm_main;

{$mode objfpc}{$H+}

interface

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  SysUtils,
  Classes,
  fpg_base,
  fpg_main,
  {%units 'Auto-generated GUI code'}
  fpg_form, fpg_grid, fpg_combobox,
  {%endunits}
  fpg_menu,
  fpg_basegrid,
  {tiOPF}
  tiModelMediator,
  Person_BOM,
  frm_person;

type

  { TMainForm }

  TMainForm = class(TfpgForm)
  private
    {@VFD_HEAD_BEGIN: frm_main}
    PersonGrid: TfpgStringGrid;
    MainMenu: TfpgMenuBar;
    menuFile: TfpgPopupMenu;
    menuNewEdit: TfpgPopupMenu;
    ComboBox1: TfpgComboBox;
    {@VFD_HEAD_END: frm_main}
    FMediator: TtiModelMediator;
    FPersonList: TPersonList;
    procedure FormShow(Sender: TObject);
    procedure menuFileExit(Sender: TObject);
    procedure menuAddEditPerson(Sender: TObject);
    procedure SetupMediators;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure AfterCreate; override;
  end;

{@VFD_NEWFORM_DECL}

implementation

uses
  //tiOIDGUID,
  tiBaseMediator,
  tiListMediators,
  tiMediators,
  //tiDialogs,
  fpg_dialogs,
  tiObject
  //tiUtils
  ;

{@VFD_NEWFORM_IMPL}

procedure TMainForm.SetupMediators;
begin
  if not Assigned(FMediator) then
  begin
    FMediator := TtiModelMediator.Create(Self);
    FMediator.AddComposite('FirstName(130);LastName(130)',PersonGrid);
    FMediator.AddProperty('FirstName',ComboBox1);
  end;
    FMediator.Subject := FPersonList;
    FMediator.Active := True;
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  SetupMediators;
  FPersonList.Read;
end;

procedure TMainForm.menuFileExit(Sender: TObject);
begin
  Close;
end;


procedure TMainForm.menuAddEditPerson(Sender: TObject);
var
  obj: TPerson;
begin
  obj := TPerson.CreateNew;
   if AddEditPerson(obj) then
   begin
     FPersonList.BeginUpdate;
     FPersonList.Add(obj);
     obj.Save;
     FPersonList.EndUpdate;
    //FPersonList.Read;
  end
  else
    obj.Free;
  //obj := TPerson.CreateNew; //==> Works fine
  //obj.FirstName := 'Niels';
  //obj.LastName := 'Nielsen';
  //FPersonList.Add(obj);
  //obj.Save;
end;

//procedure TMainForm.menuAddEditPerson(Sender: TObject);
//var
//  obj: TPerson;
//begin
//  if PersonGrid.FocusRow < 0 then
//    begin
//      if TfpgMessageDialog.Question(ApplicationName, 'Do you want to create new person?', mbYesNo) = mbYes then
//         begin
//          obj := TPerson.CreateNew;
//          if AddEditPerson(obj) then
//            FPersonList.Add(obj)
//          else
//            obj.Free;
//         end
//      else
//        begin
//          tiAppError('You need to select a Contact first');
//          Exit;
//        end;
//    end
//  else
//    begin
//      obj := TPerson(TtiStringGridMediatorView(FMediator.FindByComponent(PersonGrid).Mediator).SelectedObject);
//      if not Assigned(obj) then
//        Exit;
//      if AddEditPerson(obj) then
//        obj.Save;
//    end;
//  obj.Free;
//end;

constructor TMainForm.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FPersonList := TPersonList.Create;
  OnShow := @FormShow;
end;

destructor TMainForm.Destroy;
begin
  FMediator.Active := False;
  FPersonList.Free;
  inherited Destroy;
end;

procedure TMainForm.AfterCreate;
begin
  {%region 'Auto-generated GUI code' -fold}
  {@VFD_BODY_BEGIN: frm_main}
  Name := 'frm_main';
  SetPosition(312, 193, 553, 407);
  WindowTitle := 'frm_main';
  IconName := '';
  Hint := '';
  BackGroundColor := $80000001;

  PersonGrid := TfpgStringGrid.Create(self);
  with PersonGrid do
  begin
    Name := 'PersonGrid';
    SetPosition(4, 36, 545, 326);
    Anchors := [anLeft,anRight,anTop,anBottom];
    BackgroundColor := TfpgColor($80000002);
    FontDesc := '#Grid';
    HeaderFontDesc := '#GridHeader';
    Hint := '';
    RowCount := 0;
    ColumnCount := 0;
    RowSelect := False;
    TabOrder := 1;
    Options := [go_AlternativeColor, go_SmoothScroll];
  end;

  MainMenu := TfpgMenuBar.Create(self);
  with MainMenu do
  begin
    Name := 'MainMenu';
    SetPosition(0, 0, 555, 24);
    Anchors := [anLeft,anRight,anTop];
  end;

  menuFile := TfpgPopupMenu.Create(self);
  with menuFile do
  begin
    Name := 'menuFile';
    SetPosition(344, 136, 120, 20);
    AddMenuItem('E&xit', 'Alt+F4', @menuFileExit);
  end;

  menuNewEdit := TfpgPopupMenu.Create(self);
  with menuNewEdit do
  begin
    Name := 'menuNewEdit';
    SetPosition(344, 156, 120, 20);
    AddMenuItem('A&dd Person', 'Ctrl+A', @menuAddEditPerson);
  end;

  ComboBox1 := TfpgComboBox.Create(self);
  with ComboBox1 do
  begin
    Name := 'ComboBox1';
    SetPosition(4, 370, 545, 23);
    Anchors := [anLeft,anRight,anBottom];
    ExtraHint := '';
    FontDesc := '#List';
    Hint := '';
    FocusItem := -1;
    TabOrder := 5;
  end;

  {@VFD_BODY_END: frm_main}
  {%endregion}

  // setup main menu
  MainMenu.AddMenuItem('&File', nil).SubMenu := menuFile;
  MainMenu.AddMenuItem('&New', nil).SubMenu := menuNewEdit;
end;


Initialization

  RegisterFallBackListMediators;
  RegisterFallBackMediators;

end.
