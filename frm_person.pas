unit frm_person;

{$mode objfpc}{$H+}

interface

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  SysUtils, Classes, fpg_base, fpg_main,
  {%units 'Auto-generated GUI code'}
  fpg_form, fpg_label, fpg_edit, fpg_button,
  {%endunits}
  Person_BOM
  ,tiModelMediator;

type

  { Tfrm_Person }

  Tfrm_Person = class(TfpgForm)
  private
    {@VFD_HEAD_BEGIN: frm_person}
    lblFirstName: TfpgLabel;
    lblLastName: TfpgLabel;
    //lblError: TfpgLabel;
    edtFirstName: TfpgEdit;
    edtLastName: TfpgEdit;
    btnSavePerson: TfpgButton;
    {@VFD_HEAD_END: frm_person}
    FData: TPerson;
    FMediator: TtiModelMediator;
    procedure SetData(const AValue: TPerson);
    //procedure FormIsValid(Sender: TObject);
    procedure SetupMediators;
    procedure CheckErrors(Sender: TObject);
  public
    procedure AfterCreate; override;
    property  Data: TPerson read FData write SetData;
  end;

{@VFD_NEWFORM_DECL}

function AddEditPerson(AData: TPerson): Boolean;

implementation

uses
  //tiListMediators,
  tiBaseMediator,
  tiObject,
  tiDialogs;


function AddEditPerson(AData: TPerson): Boolean;
var
  frm: Tfrm_Person;
begin
  frm:= Tfrm_Person.Create(nil);
  try
    frm.SetData(AData);
    result:= frm.ShowModal = mrOK;
  finally
    frm.Free;
  end;
end;

{@VFD_NEWFORM_IMPL}

procedure Tfrm_Person.SetData(const AValue: TPerson);
begin
   if FData=AValue then exit;
   FData:=AValue;
   SetupMediators;
end;

//procedure Tfrm_Person.FormIsValid(Sender: TObject);
//var
//  ES: string;
//begin
//  FData.IsValid(ES);
//  lblError.Text := ES;
//  btnSavePerson.Enabled := FData.IsValid;
//  //tiAppError('You need to select a Contact first');
//  //edtFirstName.ON
//end;



procedure Tfrm_Person.SetupMediators;
begin
  if not Assigned(FMediator) then
  begin
    FMediator := TtiModelMediator.Create(self);
    FMediator.AddProperty('FirstName', edtFirstName);
    FMediator.AddProperty('LastName', edtLastName);
  end;
  FMediator.Subject := FData;
  FMediator.Active := True;
  //FormIsValid(self);
end;

procedure Tfrm_Person.CheckErrors(Sender: TObject);
begin
   if FData.IsValid then
      begin
        btnSavePerson.ModalResult := mrOK;
//        Tfrm_Person.FModalResult := mrOK;
      end
   else
       begin
         tiAppError('Please fill in missing data');
       end;
end;

procedure Tfrm_Person.AfterCreate;
begin
  {%region 'Auto-generated GUI code' -fold}
  {@VFD_BODY_BEGIN: frm_person}
  Name := 'frm_person';
  SetPosition(617, 329, 431, 70);
  WindowTitle := 'frm_person';
  IconName := '';
  Hint := '';
  BackGroundColor := $80000001;
  ShowHint := True;

  lblFirstName := TfpgLabel.Create(self);
  with lblFirstName do
  begin
    Name := 'lblFirstName';
    SetPosition(8, 12, 80, 14);
    FontDesc := '#Label1';
    Hint := '';
    Text := 'First name:';
  end;

  lblLastName := TfpgLabel.Create(self);
  with lblLastName do
  begin
    Name := 'lblLastName';
    SetPosition(8, 44, 80, 14);
    FontDesc := '#Label1';
    Hint := '';
    Text := 'Last name:';
  end;

  //lblError := TfpgLabel.Create(self);
  //with lblError do
  //begin
  //  Name := 'lblError';
  //  SetPosition(224, 36, 200, 32);
  //  FontDesc := '#Label1';
  //  Hint := '';
  //  Text := 'Errors:';
  //  BackGroundColor := $FFFF66;
  //end;

  edtFirstName := TfpgEdit.Create(self);
  with edtFirstName do
  begin
    Name := 'edtFirstName';
    SetPosition(92, 8, 120, 24);
    ExtraHint := '';
    FontDesc := '#Edit1';
    Hint := '';
    TabOrder := 3;
    Text := '';
    //OnChange := @FormIsValid;
  end;

  edtLastName := TfpgEdit.Create(self);
  with edtLastName do
  begin
    Name := 'edtLastName';
    SetPosition(92, 36, 120, 24);
    ExtraHint := '';
    FontDesc := '#Edit1';
    Hint := '';
    TabOrder := 4;
    Text := '';
    //OnEnter := @FormIsValid;
  end;

  btnSavePerson := TfpgButton.Create(self);
  with btnSavePerson do
  begin
    Name := 'btnSavePerson';
    SetPosition(224, 8, 80, 22);
    FontDesc := '#Label1';
    Hint := '';
    ImageName := '';
    //ModalResult := mrOK;
    TabOrder := 5;
    Text := 'Save';
    OnClick := @CheckErrors;
  end;

  {@VFD_BODY_END: frm_person}
  {%endregion}
end;


end.
