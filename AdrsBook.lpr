program AdrsBook;

{$mode objfpc}{$H+}
{$define UseSqldbIB}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes,
  SysUtils,
  fpg_base,
  fpg_main,
  fpg_form,
  frm_main,
  frm_person,
  Person_BOM,
  person_SrvAutoMap,
  tiINI,
  tiOPFManager,
  tiConstants,
  tiUtils,
  tiDialogs,
  strutils;

procedure MainProc;
var
  frm: TMainForm;
  dbName,dbUName,dbPass: string;
begin
  fpgApplication.Initialize;

  {$IFDEF UseSqldbIB}
  if GTIOPFManager.PersistenceLayers.FindByPersistenceLayerName(cTIPersistSqldbIB) = nil then
    raise Exception.Create('The system failed to find the <' + cTIPersistSqldbIB + '> persistence layer')
  else
    GTIOPFManager.DefaultPersistenceLayerName := cTIPersistSqldbIB;
  {$ENDIF}


  dbName := ExpandFileName(tiFixPathDelim('..\ADRS.FDB'));
  dbUName := 'sysdba';
  dbPass := 'masterkey';

  //gINI.WriteString('Database', 'dbName', dbName);
  //gINI.WriteString('Database', 'dbUName', dbUName);
  //gINI.WriteString('Database', 'dbPass', XorEncode('FPC',dbPass));
  try;
    gTIOPFManager.ConnectDatabase(dbName, dbUName,dbPass);
  except
    on Exception do
       tiAppMessage('Database not found exists.')
  end;

  Person_SrvAutoMap.RegisterMappings;

  frm := TMainForm.Create(nil);
  try
    frm.Show;
    fpgApplication.Run;
  finally
    frm.Free;
    gTIOPFManager.DisconnectDatabase;
    gTIOPFManager.Terminate;
  end;
end;

begin
  MainProc;
end.

