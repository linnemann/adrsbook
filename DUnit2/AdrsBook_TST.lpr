{ Sample FPTest project

  Lazarus IDE users
  -----------------
  If you are using the fpGUI GUI Test Runner (the only GUI Runner supported
  at the moment), remember to add fptest_fpgui.lpk to Project Inspector's
  Required Packages list. That way Lazarus IDE can find the FPTest source code.

  Any other users
  ---------------
  Remember to add the following paths to your Unit Search path settings:
     <fptest>/src
     <fptest>/src/fpGUI
     <fptest>/3rdparty/epiktimer
}
program AdrsBook_TST;

{$Mode objfpc}{$H+}

// decide what test runner to use... GUI or Text
{.$Define TextRunner}
{$Define GUIRunner}
{$define UseSqldbIB}


{$ifdef GUIRunner}
  {$IFDEF WINDOWS}
    {$apptype gui}
  {$ENDIF}
{$endif}


uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  {$IFDEF TextRunner}
  TextTestRunner,
  {$ENDIF}
  {$IFDEF GUIRunner}
  GUITestRunner,
  Person_TST,
  {$ENDIF}
  Classes,
  SysUtils,
  tiOPFManager,
  tiConstants,
  Person_BOM,
  tiUtils,
  person_SrvAutoMap,
  Person_TSTSetup,
  PersonUnitTestConstants;

begin
  // Register all tests here
  GTIOPFManager.DefaultPersistenceLayerName := cTIPersistSqldbIB;
  GTIOPFManager.ConnectDatabase(tiGetEXEPath +'../../TEST.fdb', 'SYSDBA', 'masterkey');
  Person_SrvAutoMap.RegisterMappings;
  Person_TST.RegisterTests;
  GUITestRunner.RunRegisteredTests;
end.


