unit Person_TSTSetup;

{$mode objfpc}{$H+}

interface

uses
  TestFramework,
  Person_BOM,
  SysUtils,
  tiQuery,
  tiOPFManager;

type

  { TPersonSetup }

  TPersonSetup = class(TTestCase)
    public
      procedure PersonAssign(const APerson: TPerson; const AOID: string);
      function PersonCreate(const AOID: string): TPerson;
      procedure PersonInsert(const AOID: string);
      procedure PersonCheck(const APerson: TPerson; const AOID: string);
  end;

  //TPersonTestCase = class(TTestCase)
  //  private
  //    FPersonSetup: TPersonSetup;
  //  protected
  //    procedure SetupOnce; override;
  //    procedure TearDownOnce; override;
  //    procedure Setup; override;
  //end;

implementation

{ TPersonSetup }

procedure TPersonSetup.PersonAssign(const APerson: TPerson; const AOID: string);
begin
  APerson.FirstName := IntToStr(StrToInt(AOID)+1);
  APerson.LastName := IntToStr(StrToInt(AOID)+2);
  APerson.Title := IntToStr(StrToInt(AOID)+3);
  APerson.Initials := IntToStr(StrToInt(AOID)+4);
end;

function TPersonSetup.PersonCreate(const AOID: string): TPerson;
begin
  result := TPerson.Create;
  result.OID.AsString := AOID;
  PersonAssign(result,AOID);
end;

procedure TPersonSetup.PersonInsert(const AOID: string);
var
  LPerson: TPerson;
  LParams: TtiQueryParams;
begin
  LPerson := nil;
  LParams := nil;
  try
    LPerson := PersonCreate(AOID);
    LParams := TtiQueryParams.Create;
    LParams.SetValueAsString('oid', LPerson.OID.AsString);
    LParams.SetValueAsString('first_name', LPerson.FirstName);
    LParams.SetValueAsString('last_name', LPerson.LastName);
    LParams.SetValueAsString('title',LPerson.Title);
    LParams.SetValueAsString('initials', LPerson.Initials);
    GTIOPFManager.InsertRow('person', LParams);
  finally
    LPerson.Free;
    LParams.Free;
  end;

end;

procedure TPersonSetup.PersonCheck(const APerson: TPerson; const AOID: string);
var
  LPerson: TPerson;
begin
     LPerson := PersonCreate(AOID);
  try
    CheckEquals(LPerson.FirstName, APerson.FirstName);
    CheckEquals(LPerson.LastName, APerson.LastName);
    CheckEquals(LPerson.Title, APerson.Title);
    CheckEquals(LPerson.Initials, APerson.Initials);
  finally
    LPerson.Free;
  end;
end;

end.

