unit Person_TST;

{$mode objfpc}{$H+}

interface

uses
  TestFramework,
  Person_TSTSetup;

type

  { TPersonTestCase }

  TPersonTestCase = class(TTestCase)
  private
    FPersonSetup: TPersonSetup;
  protected
    procedure SetupOnce; override;
    procedure TearDownOnce; override;
    procedure Setup; override;
    property  PersonSetup: TPersonSetup read FPersonSetup;
  published
    procedure Person_Read;
    procedure Person_Create;
    procedure Person_Update;
    procedure Person_Delete;
  end;

procedure RegisterTests;

implementation

uses
  Person_BOM,
  //tiDialogs,
  tiOPFManager,
  PersonUnitTestConstants;

procedure RegisterTests;
begin
  TestFramework.RegisterTest(TPersonTestCase.Suite);
end;

{ TPersonTestCase }

procedure TPersonTestCase.Person_Read;
var
  LList: TPersonList;
begin
  PersonSetup.PersonInsert(cOIDPerson1);
  LList := TPersonList.Create;
  try
    LList.Read;
    CheckEquals(1, LList.Count);
    PersonSetup.PersonCheck(LList.Items[0], cOIDPerson1);
    CheckEquals(cOIDPerson1, LList.Items[0].OID.AsString);
  finally
    LList.Free;
  end;
end;

procedure TPersonTestCase.Person_Create;
var
  LList: TPersonList;
  LItem: TPerson;
begin
  LItem:= PersonSetup.PersonCreate(cOIDPerson1);
  try
    LItem.Dirty:= True;
    LItem.Save;
  finally
    LItem.Free;
  end;

  LList := TPersonList.Create;
  try
    LList.Read;
    PersonSetup.PersonCheck(LList.Items[0], cOIDPerson1);
    CheckEquals(cOIDPerson1, LList.Items[0].OID.AsString);
  finally
    LList.Free;
  end;
end;

procedure TPersonTestCase.Person_Update;
var
  LList: TPersonList;

  begin
    PersonSetup.PersonInsert(cOIDPerson1);
    LList := TPersonList.Create;
    try
      LList.Read;
      PersonSetup.PersonCheck(LList.Items[0],cOIDPerson1);
      PersonSetup.PersonAssign(LList.Items[0],cOIDPerson2);
      LList.Items[0].Dirty := True;
      LList.Items[0].Save;
    finally
      LList.Free;
    end;

    LList := TPersonList.Create;
    try
      LList.Read;
      PersonSetup.PersonCheck(LList.Items[0], cOIDPerson2);
    finally
      LList.Free;
    end;
end;

procedure TPersonTestCase.Person_Delete;
var
  LList: TPersonList;

begin
  PersonSetup.PersonInsert(cOIDPerson1);
  LList := TPersonList.Create;

  try
    LList.Read;
    LList.Items[0].Deleted := True;
    LList.Items[0].Save;
  finally
    LList.Free;
  end;

  LList := TPersonList.Create;
  try
    LList.Read;
    CheckEquals(0, LList.Count);
  finally
    LList.Free;
  end;


end;

procedure TPersonTestCase.SetupOnce;
begin
  inherited;
  FPersonSetup := TPersonSetup.Create;
end;

procedure TPersonTestCase.TearDownOnce;
begin
  FPersonSetup.Free;
  inherited;
end;

procedure TPersonTestCase.Setup;
begin
  inherited;
  GTIOPFManager.DeleteRow('person', nil);
end;

end.

